<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('test_number')->unique();
            $table->date('test_date')->nullable();
            $table->enum('type', ['PAID', 'FREE']);
            $table->enum('test_state', ['DRAFT', 'FINISH']);
            $table->enum('status', ['INV', 'PAI', 'CHK', 'REJ', 'EXP'])->nullable();// invoice, paid, check, rejected, expired
            $table->decimal('price',15,2)->default(0);
            $table->dateTime('paid_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
