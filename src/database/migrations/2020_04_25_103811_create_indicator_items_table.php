<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicatorItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicator_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('indicator_id');
            $table->char('code');
            $table->string('name');
            $table->string('acronym');
            $table->integer('order');
            $table->char('option', 1);
            $table->boolean('is_active')->default(1);
            $table->timestamps();

            $table->foreign('indicator_id')->references('id')->on('indicators');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicator_items');
    }
}
